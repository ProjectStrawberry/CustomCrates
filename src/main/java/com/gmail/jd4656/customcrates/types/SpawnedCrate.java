package com.gmail.jd4656.customcrates.types;

import com.gmail.jd4656.customcrates.CustomCrates;
import org.bukkit.*;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class SpawnedCrate extends Crate {
    Location location;
    long createdAt;
    UUID armorStandId;
    Location armorStandLoc;
    boolean locked = true;
    Player unlockingPlayer;
    int unlockProgress = 0;
    BukkitTask task;
    BossBar bossBar;

    public SpawnedCrate(String name, String groupId) {
        super(name, groupId);
        this.createdAt = System.currentTimeMillis();
    }

    public void destroy() {
        location.getBlock().setType(Material.AIR);
        Collection<Entity> entityList = location.getWorld().getNearbyEntities(armorStandLoc, 1, 1, 1);
        for (Entity entity : entityList) {
            if (entity.getUniqueId().equals(armorStandId)) {
                entity.remove();
                break;
            }
        }
    }

    public BukkitTask getTask() {
        return this.task;
    }

    public void setTask(BukkitTask task) {
        this.task = task;
    }

    public BossBar getBossBar() {
        return this.bossBar;
    }

    public void setBossBar(BossBar bossBar) {
        this.bossBar = bossBar;
    }

    public int getProgress() {
        return unlockProgress;
    }

    public void addProgress(int seconds) {
        unlockProgress += seconds;

        if (unlockProgress >= this.getGroup().getLockTimer()) {
            this.locked = false;
            unlockingPlayer.sendMessage(ChatColor.YELLOW + "You've finished unlocking this crate, you can now punch it to open it.");
            this.bossBar.removeAll();
            Bukkit.removeBossBar(new NamespacedKey(CustomCrates.plugin, "crate-" + location.getBlockX() + "-" + location.getBlockY() + "-" + location.getBlockZ()));
            this.task.cancel();
        }
    }

    public Player getUnlockingPlayer() {
        return unlockingPlayer;
    }

    public void setUnlockingPlayer(Player player) {
        unlockingPlayer = player;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location loc) {
        this.location = loc;
    }

    public void setArmorStandLoc(Location loc) {
        armorStandLoc = loc;
    }

    public Location getArmorStandLoc() {
        return this.armorStandLoc;
    }

    public void setArmorStandId(UUID id) {
        this.armorStandId = id;
    }

    public UUID getArmorStandId() {
        return this.armorStandId;
    }

    public boolean isLocked() {
        return this.locked;
    }
}
