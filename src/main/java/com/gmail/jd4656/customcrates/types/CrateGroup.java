package com.gmail.jd4656.customcrates.types;

import com.gmail.jd4656.customcrates.CustomCrates;
import org.bukkit.boss.BarColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class CrateGroup {
    private String displayName, id;
    private BarColor barColor = BarColor.GREEN;
    private int lockTimer = 90;
    private List<Crate> crates = new ArrayList<>();
    private Random rand = new Random();

    public CrateGroup(String id) {
        this.id = id;
        this.displayName = id;
    }

    public void setLockTimer(int lockTimer) {
        this.lockTimer = lockTimer;
    }

    public int getLockTimer() {
        return this.lockTimer;
    }

    public String getId() {
        return this.id;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String name) {
        this.displayName = name;
    }

    public BarColor getBarColor() {
        return this.barColor;
    }

    public void setBarColor(String color) {
        barColor = BarColor.valueOf(color);
    }

    public List<String> getCrateIds() {
        return crates.stream().map(Crate::getId).collect(Collectors.toList());
    }

    public void addCrate(Crate crate) {
        crates.add(crate);
    }

    public boolean isEmpty() {
        return crates.isEmpty();
    }

    public void removeCrate(Crate crate) {
        CustomCrates.db.deleteCrate(crate);
        crates.remove(crate);
    }

    public Crate getCrate(String id) {
        for (Crate crate : crates) {
            if (crate.getId().equals(id)) return crate;
        }

        return null;
    }

    public Crate getRandomCrate() {
        return crates.get(rand.nextInt(crates.size()));
    }
}
