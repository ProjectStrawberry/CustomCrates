package com.gmail.jd4656.customcrates.types;

import com.gmail.jd4656.customcrates.CustomCrates;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Crate {
    String name, id, groupId;

    List<ItemStack> items = new ArrayList<>();

    public Crate(String id, String groupId) {
        this.id = id.toLowerCase();
        this.name = id;
        this.groupId = groupId;
    }

    public String getItemsAsBase64() {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            // Write the size of the inventory
            dataOutput.writeInt(items.size());

            // Save every element in the list
            for (ItemStack item : items) {
                dataOutput.writeObject(item);
            }

            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            return null;
        }
    }

    public void setItemsFromBase64(String base64) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(base64));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack[] tempItems = new ItemStack[dataInput.readInt()];

            // Read the serialized inventory
            for (int i = 0; i < tempItems.length; i++) {
                tempItems[i] = (ItemStack) dataInput.readObject();
            }

            dataInput.close();

            items.addAll(Arrays.asList(tempItems));
        } catch (IOException | ClassNotFoundException ignored) {}
    }

    public void setItems(Collection<ItemStack> itemsToAdd) {
        items.clear();
        items.addAll(itemsToAdd);
    }

    public List<ItemStack> getItems() {
        return items;
    }
    public String getId() {
        return this.id;
    }

    public String getGroupId() {
        return this.groupId;
    }

    public CrateGroup getGroup() {
        return CustomCrates.plugin.crateManager.getGroup(this.groupId);
    }

    public String getName() {
        return this.name;
    }
}
