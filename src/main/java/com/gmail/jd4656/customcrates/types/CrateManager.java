package com.gmail.jd4656.customcrates.types;

import com.gmail.jd4656.customcrates.CustomCrates;
import com.gmail.jd4656.customcrates.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.*;

public class CrateManager {
    private CustomCrates plugin;
    private Map<Location, SpawnedCrate> spawnedCrates = new HashMap<>();
    private Map<String, CrateGroup> groups = new HashMap<>();

    public CrateManager(CustomCrates p) {
        plugin = p;

        CustomCrates.db.loadGroups().forEach(group -> groups.put(group.getId(), group));
        CustomCrates.db.loadCrates().forEach(crate -> groups.get(crate.getGroupId()).addCrate(crate));
        CustomCrates.db.loadActiveCrates().forEach(crate -> spawnedCrates.put(crate.getLocation(), crate));
    }

    public List<String> getCrateIds() {
        List<String> crateIds = new ArrayList<>();
        for (CrateGroup group : groups.values()) {
            crateIds.addAll(group.getCrateIds());
        }

        return crateIds;
    }

    public Collection<SpawnedCrate> getActiveCrates() {
        return spawnedCrates.values();
    }

    public Set<String> getGroupIds() {
        return groups.keySet();
    }

    public void addGroup(CrateGroup group) {
        groups.put(group.getId(), group);
    }

    public void addCrate(Crate crate) {
        groups.get(crate.getGroupId()).addCrate(crate);
    }

    public void removeCrate(Crate crate) {
        groups.get(crate.getGroupId()).removeCrate(crate);
        if (groups.get(crate.getGroupId()).isEmpty()) {
            CustomCrates.db.deleteGroup(groups.get(crate.getGroupId()));
            groups.remove(crate.getGroupId());
        }
    }

    public CrateGroup getGroup(String id) {
        return groups.get(id);
    }

    public Crate getCrateById(String id) {
        for (CrateGroup group : groups.values()) {
            Crate crate = group.getCrate(id);
            if (crate != null) return crate;
        }

        return null;
    }

    public void removeActiveCrate(Location loc) {
        if (!spawnedCrates.containsKey(loc)) return;
        CustomCrates.db.removeActiveCrate(spawnedCrates.get(loc));
        spawnedCrates.remove(loc);
    }

    public SpawnedCrate getActiveCrate(Location loc) {
        return spawnedCrates.get(loc);
    }

    public void spawnCrate(Crate crate, Location loc) {
        SpawnedCrate sCrate = new SpawnedCrate(crate.getName(), crate.getGroupId());
        sCrate.setItems(crate.getItems());
        sCrate.setLocation(loc);

        loc.getBlock().setType(Material.CHEST);

        Location armorStandLoc = loc.clone().add(0.5, -1, 0.5);

        ArmorStand armorStand = (ArmorStand) loc.getWorld().spawnEntity(armorStandLoc, EntityType.ARMOR_STAND);
        armorStand.setVisible(false);
        armorStand.setCustomName(ChatColor.RED + Utils.parseColorCodes(sCrate.getGroup().getDisplayName() + " Crate"));
        armorStand.setCustomNameVisible(true);

        sCrate.setArmorStandLoc(armorStandLoc);
        sCrate.setArmorStandId(armorStand.getUniqueId());

        spawnedCrates.put(loc, sCrate);
        CustomCrates.db.saveActiveCrate(sCrate);
    }
}
