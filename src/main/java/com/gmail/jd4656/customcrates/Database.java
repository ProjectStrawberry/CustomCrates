package com.gmail.jd4656.customcrates;

import com.gmail.jd4656.customcrates.types.Crate;
import com.gmail.jd4656.customcrates.types.CrateGroup;
import com.gmail.jd4656.customcrates.types.SpawnedCrate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Database {
    private File dbFile;
    private Connection conn;
    private CustomCrates plugin;

    public Database(CustomCrates p) {
        plugin = p;

        dbFile = new File(plugin.getDataFolder(), "plugin.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: plugin.db");
                return;
            }
        }

        try {
            Connection c = this.getConnection();
            Statement stmt = c.createStatement();

            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS crates (crateId TEXT, crateName TEXT, groupId TEXT, items TEXT)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS groups (id TEXT, displayName TEXT, barColor TEXT, lockTimer INTEGER)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS activeCrates (crateName TEXT, groupId TEXT, items TEXT, x INTEGER, y INTEGER, z INTEGER, world TEXT," +
                    "armorStandX INTEGER, armorStandY INTEGER, armorStandZ INTEGER, armorStandId TEXT)");
            stmt.close();
        } catch (Exception e) {
            plugin.getLogger().warning("Error creating tables:");
            e.printStackTrace();
        }
    }

    public List<CrateGroup> loadGroups() {
        List<CrateGroup> groups = new ArrayList<>();
        try {
            Connection c = getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM groups");

            while (rs.next()) {
                CrateGroup group = new CrateGroup(rs.getString("id"));
                group.setDisplayName(rs.getString("displayName"));
                group.setBarColor(rs.getString("barColor"));
                group.setLockTimer(rs.getInt("lockTimer"));
                groups.add(group);
            }

            rs.close();
            stmt.close();

            return groups;
        } catch (Exception e) {
            plugin.getLogger().warning("Error loading groups:");
            e.printStackTrace();
        }

        return null;
    }

    public List<Crate> loadCrates() {
        List<Crate> crates = new ArrayList<>();
        try {
            Connection c = getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM crates");

            while (rs.next()) {
                Crate crate = new Crate(rs.getString("crateName"), rs.getString("groupId"));
                crate.setItemsFromBase64(rs.getString("items"));
                crates.add(crate);
            }

            rs.close();
            stmt.close();
        } catch (Exception e) {
            plugin.getLogger().warning("Error loading crates:");
            e.printStackTrace();
        }

        return crates;
    }

    public List<SpawnedCrate> loadActiveCrates() {
        List<SpawnedCrate> crates = new ArrayList<>();
        try {
            Connection c = getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM activeCrates");

            while (rs.next()) {
                SpawnedCrate crate = new SpawnedCrate(rs.getString("crateName"), rs.getString("groupId"));
                crate.setItemsFromBase64(rs.getString("items"));

                World crateWorld = Bukkit.getWorld(rs.getString("world"));
                if (crateWorld == null) continue;

                crate.setLocation(new Location(crateWorld, rs.getInt("x"), rs.getInt("y"), rs.getInt("z")));
                crate.setArmorStandLoc(new Location(crateWorld, rs.getInt("armorStandX"), rs.getInt("armorStandY"), rs.getInt("armorStandZ")));
                crate.setArmorStandId(UUID.fromString(rs.getString("armorStandId")));

                crates.add(crate);
            }

            rs.close();
            stmt.close();
        } catch (Exception e) {
            plugin.getLogger().warning("Error loading active crates:");
            e.printStackTrace();
        }

        return crates;
    }


    public void deleteCrate(Crate crate) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = getConnection();
                    PreparedStatement pstmt = c.prepareStatement("DELETE FROM crates WHERE crateId=?");
                    pstmt.setString(1, crate.getId());
                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error saving crates:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public void deleteGroup(CrateGroup group) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = getConnection();
                    PreparedStatement pstmt = c.prepareStatement("DELETE FROM groups WHERE id=?");
                    pstmt.setString(1, group.getId());
                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error saving crates:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public void saveGroup(CrateGroup group) {
        // CREATE TABLE IF NOT EXISTS groups (id TEXT, displayName TEXT, barColor TEXT
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = getConnection();
                    PreparedStatement pstmt = c.prepareStatement("DELETE FROM groups WHERE id=?");
                    pstmt.setString(1, group.getId());
                    pstmt.executeUpdate();

                    pstmt = c.prepareStatement("INSERT INTO groups (id, displayName, barColor, lockTimer) VALUES (?, ?, ?, ?)");
                    pstmt.setString(1, group.getId());
                    pstmt.setString(2, group.getDisplayName());
                    pstmt.setString(3, group.getBarColor().toString());
                    pstmt.setInt(4, group.getLockTimer());
                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error saving crates:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public void saveCrate(Crate crate) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    plugin.getLogger().info("saving crate: " + crate.getId());
                    Connection c = getConnection();
                    PreparedStatement pstmt = c.prepareStatement("DELETE FROM crates WHERE crateId=?");
                    pstmt.setString(1, crate.getId());
                    pstmt.executeUpdate();

                    pstmt = c.prepareStatement("INSERT INTO crates (crateId, crateName, groupId, items) VALUES (?, ?, ?, ?)");
                    pstmt.setString(1, crate.getId());
                    pstmt.setString(2, crate.getName());
                    pstmt.setString(3, crate.getGroupId());
                    pstmt.setString(4, crate.getItemsAsBase64());
                    pstmt.executeUpdate();

                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error saving crates:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public void saveActiveCrate(SpawnedCrate crate) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = getConnection();
                    PreparedStatement  pstmt = c.prepareStatement("INSERT INTO activeCrates (crateName, groupId, items," +
                            " x, y, z, world, armorStandX, armorStandY, armorStandZ, armorStandId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    pstmt.setString(1, crate.getName());
                    pstmt.setString(2, crate.getGroupId());
                    pstmt.setString(3, crate.getItemsAsBase64());
                    pstmt.setInt(4, crate.getLocation().getBlockX());
                    pstmt.setInt(5, crate.getLocation().getBlockY());
                    pstmt.setInt(6, crate.getLocation().getBlockZ());
                    pstmt.setString(7, crate.getLocation().getWorld().getName());
                    pstmt.setInt(8, crate.getArmorStandLoc().getBlockX());
                    pstmt.setInt(9, crate.getArmorStandLoc().getBlockY());
                    pstmt.setInt(10, crate.getArmorStandLoc().getBlockZ());
                    pstmt.setString(11, crate.getArmorStandId().toString());
                    pstmt.executeUpdate();

                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error saving active crates:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public void removeActiveCrate(SpawnedCrate crate) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = getConnection();
                    PreparedStatement  pstmt = c.prepareStatement("DELETE FROM activeCrates WHERE x=? AND y=? AND z=?");
                    pstmt.setInt(1, crate.getLocation().getBlockX());
                    pstmt.setInt(2, crate.getLocation().getBlockY());
                    pstmt.setInt(3, crate.getLocation().getBlockZ());
                    pstmt.executeUpdate();

                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error removing active crates:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
        }
        return conn;
    }
}
