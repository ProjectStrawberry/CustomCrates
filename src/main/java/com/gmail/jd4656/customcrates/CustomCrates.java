package com.gmail.jd4656.customcrates;

import co.aikar.commands.PaperCommandManager;
import com.gmail.jd4656.customcrates.types.CrateManager;
import com.gmail.jd4656.customcrates.ui.UIManager;
import org.bukkit.boss.BarColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.popcraft.chunkyborder.ChunkyBorder;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CustomCrates extends JavaPlugin {
    public static CustomCrates plugin;
    public CrateManager crateManager;
    public UIManager uiManager;
    public static Database db;
    static ChunkyBorder chunkyBorder = null;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        chunkyBorder = ((ChunkyBorder) getServer().getPluginManager().getPlugin("ChunkyBorder"));

        if (chunkyBorder == null) {
            getLogger().warning("Unabled to find ChunkyBorder - disabling plugin");
            return;
        }

        db = new Database(this);
        crateManager = new CrateManager(this);
        PaperCommandManager commandManager = new PaperCommandManager(this);
        commandManager.registerCommand(new CrateCommand(this));
        commandManager.enableUnstableAPI("help");
        uiManager = new UIManager(this);

        commandManager.getCommandCompletions().registerCompletion("crateId", c -> crateManager.getCrateIds());
        commandManager.getCommandCompletions().registerCompletion("groupId", c -> crateManager.getGroupIds());
        commandManager.getCommandCompletions().registerCompletion("barColor", c -> Arrays.stream(BarColor.values()).map(Enum::toString).collect(Collectors.toList()));

        getServer().getPluginManager().registerEvents(uiManager, this);
        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
    }
}
