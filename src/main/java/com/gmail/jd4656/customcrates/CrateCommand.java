package com.gmail.jd4656.customcrates;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Values;
import com.gmail.jd4656.customcrates.types.Crate;
import com.gmail.jd4656.customcrates.types.CrateGroup;
import com.gmail.jd4656.customcrates.types.SpawnedCrate;
import com.gmail.jd4656.customcrates.ui.EditCrate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@CommandAlias("customcrates|customcrate|crates|crate")
public class CrateCommand extends BaseCommand {
    CustomCrates plugin;

    CrateCommand(CustomCrates p) {
        plugin = p;
    }

    @Subcommand("list")
    @Description("Lists any crates that are currently in the world.")
    @CommandPermission("customcrates.use")
    public void list(CommandSender player) {
        if (plugin.crateManager.getActiveCrates().isEmpty()) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are currently no spawned crates.");
            return;
        }

        for (SpawnedCrate crate : plugin.crateManager.getActiveCrates()) {
            int randX = Math.round((float) crate.getLocation().getX() / 100f) * 100;
            int randZ = Math.round((float) crate.getLocation().getZ() / 100f) * 100;
            player.sendMessage(Utils.parseColorCodes("&4&l[&c&lCamarosa&4&l] &cThere is a " + crate.getGroup().getDisplayName() +
                    " &ccrate somewhere near &6[&3x:&b" + randX + " &3y:&b" + crate.getLocation().getBlockY() + " &3z:&b" + randZ + "&6] &cin the world &b" + crate.getLocation().getWorld().getName()));
        }
    }

    @Subcommand("create")
    @Description("Create a new crate")
    @CommandPermission("customcrates.admin")
    @CommandCompletion("<crateId> <group>")
    public void create(Player player, @Single String crateName, @Single String groupId) {
        String crateId = crateName.toLowerCase();
        if (plugin.crateManager.getCrateById(crateId) != null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A crate with that id already exists.");
            return;
        }

        groupId = groupId.toLowerCase();
        CrateGroup group = plugin.crateManager.getGroup(groupId);
        if (group == null) {
            group = new CrateGroup(groupId);
            plugin.crateManager.addGroup(group);
        }

        Crate crate = new Crate(crateName, groupId);
        plugin.crateManager.addCrate(crate);

        EditCrate editUi = new EditCrate(crate);
        plugin.uiManager.show(player, editUi);
    }

    @Subcommand("edit")
    @Description("Edit a crate")
    @CommandPermission("customcrates.admin")
    @CommandCompletion("@crateId")
    public void edit(Player player, @Single String crateId) {
        Crate crate = plugin.crateManager.getCrateById(crateId.toLowerCase());
        if (crate == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
            return;
        }

        EditCrate editUi = new EditCrate(crate);
        plugin.uiManager.show(player, editUi);
    }

    @Subcommand("drop")
    @Description("Drops a crate")
    @CommandPermission("customcrates.admin")
    @CommandCompletion("@groupId @worlds")
    public void drop(CommandSender player, @Single String groupId, @Single @Optional String worldName) {
        CrateGroup group = plugin.crateManager.getGroup(groupId.toLowerCase());
        if (group == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That group does not exist.");
            return;
        }

        if (!(player instanceof Player) && worldName == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Please specify a world name when using this command from console.");
            return;
        }

        if (player instanceof Player && worldName == null) worldName = ((Player) player).getWorld().getName();

        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid world.");
            return;
        }

        Crate crate = group.getRandomCrate();
        Location location = Utils.getRandomLocation(world);

        plugin.crateManager.spawnCrate(crate, location);

        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();

        int randX = Math.round((float) location.getX() / 100f) * 100;
        int randZ = Math.round((float) location.getZ() / 100f) * 100;

        Bukkit.broadcastMessage(Utils.parseColorCodes("&4&l[&c&lCamarosa&4&l] &cA " + group.getDisplayName() +
                " &ccrate has dropped somewhere near &6[&3x:&b" + randX + " &3y:&b" + y + " &3z:&b" + randZ + "&6] &cin the world &b" + worldName));
        player.sendMessage(ChatColor.GOLD + "Your crate has been spawned at x: " + ChatColor.RED + x + ChatColor.GOLD + " y: " + ChatColor.RED + y + ChatColor.GOLD +" z: " + ChatColor.RED + z);
    }

    @Subcommand("remove")
    @Description("Removes a crate")
    @CommandPermission("customcrates.admin")
    @CommandCompletion("@crateId")
    public void remove(CommandSender player, @Single String crateId) {
        Crate crate = plugin.crateManager.getCrateById(crateId);
        if (crate == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
            return;
        }

        for (SpawnedCrate sCrate : plugin.crateManager.getActiveCrates()) {
            if (sCrate.getId().equals(crate.getId())) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't remove a crate that is spawned in the world.");
                return;
            }
        }

        plugin.crateManager.removeCrate(crate);

        player.sendMessage(ChatColor.RED + "That crate has been removed");
    }

    @Subcommand("addchest")
    @Description("Adds the items in the chest you are looking at to a crate")
    @CommandPermission("customcrates.admin")
    @CommandCompletion("@crateId")
    public void addchest(Player player, @Single String crateId) {
        Crate crate = plugin.crateManager.getCrateById(crateId);
        if (crate == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
            return;
        }

        Block block = player.getTargetBlockExact(5);
        if (block == null || block.getType() != Material.CHEST) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must be looking at a chest for this command to work.");
            return;
        }

        Container container = (Container) block.getState();
        List<ItemStack> itemsToAdd = new ArrayList<>();
        for (ItemStack item : container.getInventory().getContents()) {
            if (item == null || item.getType() == Material.AIR) continue;
            itemsToAdd.add(item.clone());
        }

        crate.setItems(itemsToAdd);
        CustomCrates.db.saveCrate(crate);

        player.sendMessage(ChatColor.GOLD + "You've added those items to the crate");
    }

    @Subcommand("setchest")
    @Description("Fills a chest with the items from a crate")
    @CommandPermission("customcrates.admin")
    @CommandCompletion("@crateId")
    public void setchest(Player player, @Single String crateId) {
        Crate crate = plugin.crateManager.getCrateById(crateId);
        if (crate == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate does not exist.");
            return;
        }

        Block block = player.getTargetBlockExact(5);
        if (block == null || block.getType() != Material.CHEST) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must be looking at a chest for this command to work.");
            return;
        }

        if (crate.getItems().isEmpty()) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That crate has no items added.");
            return;
        }

        Container container = (Container) block.getState();
        for (ItemStack item : crate.getItems()) {
            container.getInventory().addItem(item.clone());
        }

        player.sendMessage(ChatColor.GOLD + "The items in that crate have been added to this chest.");
    }


    @Subcommand("set")
    @Description("Edit group settings")
    public class GroupCommands extends BaseCommand {
        @Subcommand("displayname")
        @Description("Sets a groups display name")
        @CommandCompletion("@groupId <displayName>")
        public void displayname(CommandSender player, @Single String groupId, String displayName) {
            CrateGroup group = plugin.crateManager.getGroup(groupId);
            if (group == null) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That group does not exist.");
                return;
            }

            group.setDisplayName(displayName);
            CustomCrates.db.saveGroup(group);

            player.sendMessage(ChatColor.GOLD + "The group " + ChatColor.RED + groupId + ChatColor.GOLD + " has had its display name set to " + Utils.parseColorCodes(displayName));
        }

        @Subcommand("barcolor")
        @Description("Sets the bar color for a group")
        @CommandCompletion("@groupId @barColor")
        public void barcolor(CommandSender player, @Single String groupId, @Values("@barColor") String barColor) {
            CrateGroup group = plugin.crateManager.getGroup(groupId);
            if (group == null) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That group does not exist.");
                return;
            }

            group.setBarColor(barColor);
            CustomCrates.db.saveGroup(group);

            player.sendMessage(ChatColor.GOLD + "The bar color has been set to " + ChatColor.RED + barColor + ChatColor.GOLD + " for that group.");
        }
    }

    @HelpCommand
    public static void onHelp(CommandHelp help) {
        help.showHelp();
    }
}
