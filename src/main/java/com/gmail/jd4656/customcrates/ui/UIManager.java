package com.gmail.jd4656.customcrates.ui;

import com.gmail.jd4656.customcrates.CustomCrates;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;

import java.util.HashMap;
import java.util.Map;

public class UIManager implements Listener {
    Map<Integer, UIBase> trackedInventories = new HashMap<>();

    CustomCrates plugin;
    public UIManager(CustomCrates p) {
        plugin = p;
    }

    @EventHandler
    public void InventoryClickEvent(InventoryClickEvent event) {
        int hashCode = event.getInventory().hashCode();
        if (!trackedInventories.containsKey(hashCode)) return;
        trackedInventories.get(hashCode).onClick(event);
    }

    @EventHandler
    public void InventoryMoveItemEvent(InventoryMoveItemEvent event) {
    }

    @EventHandler
    public void InventoryCloseEvent(InventoryCloseEvent event) {
        int hashCode = event.getInventory().hashCode();
        if (trackedInventories.containsKey(hashCode)) {
            UIBase closingInv = trackedInventories.get(hashCode);
            closingInv.onClose();
            trackedInventories.remove(hashCode);
        }
    }

    public void show(Player player, UIBase ui) {
        trackedInventories.put(ui.hashCode(), ui);
        player.openInventory(ui.getInventory());
    }
}
