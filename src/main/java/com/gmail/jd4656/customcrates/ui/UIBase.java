package com.gmail.jd4656.customcrates.ui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public abstract class UIBase {
    protected Inventory inv;

    public abstract void onClick(InventoryClickEvent event);
    public abstract void onClose();

    public Inventory getInventory() {
        return this.inv;
    }
    public int hashCode() {
        return this.inv.hashCode();
    }
}
