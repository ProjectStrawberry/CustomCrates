package com.gmail.jd4656.customcrates.ui;

import com.gmail.jd4656.customcrates.CustomCrates;
import com.gmail.jd4656.customcrates.types.Crate;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EditCrate extends UIBase {
    Crate crate;

    public EditCrate(Crate crate) {
        this.crate = crate;
        this.inv = Bukkit.createInventory(null, 27, "Editing crate " + crate.getId());

        int invPos = 0;
        for (ItemStack item : crate.getItems()) {
            inv.setItem(invPos, item);
            invPos++;
        }
    }

    @Override
    public void onClick(InventoryClickEvent event) {

    }

    @Override
    public void onClose() {
        List<ItemStack> items = new ArrayList<>();
        for (int i=0; i<27; i++) {
            ItemStack curItem = this.inv.getItem(i);
            if (curItem == null || curItem.getType().equals(Material.AIR)) continue;
            items.add(curItem);
        }

        this.crate.setItems(items);
        CustomCrates.db.saveCrate(this.crate);
    }
}
