package com.gmail.jd4656.customcrates;

import com.gmail.jd4656.customcrates.types.SpawnedCrate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class EventListeners implements Listener {

    CustomCrates plugin;
    EventListeners(CustomCrates p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!plugin.crateManager.getActiveCrates().isEmpty()) {
            for (SpawnedCrate crate : plugin.crateManager.getActiveCrates()) {
                if (crate.getBossBar() != null) {
                    crate.getBossBar().removePlayer(player);
                }
                int randX = Math.round((float) crate.getLocation().getX() / 100f) * 100;
                int randZ = Math.round((float) crate.getLocation().getZ() / 100f) * 100;
                player.sendMessage(Utils.parseColorCodes("&4&l[&c&lCamarosa&4&l] &cThere is a " + crate.getGroup().getDisplayName() +
                        " &ccrate somewhere near &6[&3x:&b" + randX + " &3y:&b" + crate.getLocation().getBlockY() + " &3z:&b" + randZ + "&6] &cin the world &b" + crate.getLocation().getWorld().getName()));
            }
        }
    }

    @EventHandler
    public void BlockBreakEvent(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (!(block.getState() instanceof Chest)) return;
        if (plugin.crateManager.getActiveCrate(block.getLocation()) != null) event.setCancelled(true);
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if (block == null) return;
        if (!(block.getState() instanceof Chest)) return;
        SpawnedCrate crate = plugin.crateManager.getActiveCrate(block.getLocation());
        if (crate == null) return;

        Location loc = block.getLocation();

        event.setCancelled(true);

        if (!crate.isLocked()) {
            for (ItemStack item : crate.getItems()) {
                if (item == null) continue;
                player.getWorld().dropItem(loc, item);
            }
            Bukkit.broadcastMessage(Utils.parseColorCodes("&4&l[&c&lCamarosa&4&l] &cThe " + crate.getGroup().getDisplayName() +
                    " &ccrate at &6[&3x:&b" + crate.getLocation().getBlockX() + " &3y:&b" + crate.getLocation().getBlockY() + " &3z:&b" + crate.getLocation().getBlockZ() +
                    "&6] &cin the world &b" + crate.getLocation().getWorld().getName() + " &chas been found by " + player.getDisplayName()));

            crate.destroy();
            plugin.crateManager.removeActiveCrate(crate.getLocation());
        } else {
            if (crate.getUnlockingPlayer() != null) return;
            crate.setUnlockingPlayer(player);
            player.sendMessage(ChatColor.YELLOW + "You're unlocking this crate - please stay within 5 blocks until you finish unlocking it.");

            BossBar bossBar = crate.getBossBar();
            if (bossBar == null) {
                int timeRemaining = (crate.getGroup().getLockTimer() - crate.getProgress());
                bossBar = Bukkit.createBossBar(new NamespacedKey(plugin, "crate-" + loc.getBlockX() + "-" + loc.getBlockY() + "-" + loc.getBlockZ()),
                        Utils.parseColorCodes("&eUnlocking " + crate.getGroup().getDisplayName() + "&e crate... " +
                                timeRemaining + (timeRemaining == 1 ? " second" : " seconds") + " remaining."), crate.getGroup().getBarColor(), BarStyle.SOLID);
                bossBar.setProgress(0);
                bossBar.setVisible(true);
            }

            bossBar.addPlayer(player);
            crate.setBossBar(bossBar);

            crate.setTask(new BukkitRunnable() {
                @Override
                public void run() {
                    if (crate.getUnlockingPlayer() == null || !crate.getUnlockingPlayer().isOnline() || crate.getUnlockingPlayer().isDead()) {
                        crate.setUnlockingPlayer(null);
                        crate.getBossBar().removeAll();
                        this.cancel();
                        return;
                    }

                    if (player.getLocation().distance(crate.getLocation()) > 5) {
                        player.sendMessage(ChatColor.YELLOW + "You're no longer within range of the crate, so unlocking has been paused.");
                        crate.setUnlockingPlayer(null);
                        crate.getBossBar().removeAll();
                        this.cancel();
                        return;
                    }

                    crate.addProgress(1);
                    crate.getBossBar().setProgress((double) crate.getProgress() / crate.getGroup().getLockTimer());
                    int timeRemaining = (crate.getGroup().getLockTimer() - crate.getProgress());
                    crate.getBossBar().setTitle(Utils.parseColorCodes("&eUnlocking " + crate.getGroup().getDisplayName() + "&e crate... " +
                            timeRemaining + (timeRemaining == 1 ? " second" : " seconds") + " remaining."));
                }
            }.runTaskTimer(plugin, 0, 20L));
        }
    }
}
