package com.gmail.jd4656.customcrates;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.popcraft.chunkyborder.BorderData;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.bukkit.ChatColor.COLOR_CHAR;

public class Utils {
    public static Location getRandomLocation(World world) {
        if (CustomCrates.chunkyBorder == null || !CustomCrates.chunkyBorder.getBorders().containsKey(world.getName())) return null;
        BorderData border = CustomCrates.chunkyBorder.getBorders().get(world.getName());
        Location center = new Location(world, border.getCenterX(), 64, border.getCenterZ());
        Random rand = new Random();
        double angle = rand.nextDouble() * 360;

        int x = (int) (center.getX() + (rand.nextDouble() * border.getRadiusX() * Math.cos(Math.toRadians(angle))));
        int z = (int) (center.getZ() + (rand.nextDouble() * border.getRadiusZ() * Math.sin(Math.toRadians(angle))));
        int y = world.getHighestBlockYAt(x, z) + 1;

        return world.getBlockAt(x, y, z).getLocation();
    }

    public static String parseColorCodes(String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);
        str = translateHexColorCodes("#", "", str);

        return str;
    }

    public static String translateHexColorCodes(String startTag, String endTag, String message) {
        final Pattern hexPattern = Pattern.compile(startTag + "([A-Fa-f0-9]{6})" + endTag);
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 4 * 8);
        while (matcher.find()) {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
                    + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
                    + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
                    + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
            );
        }
        return matcher.appendTail(buffer).toString();
    }
}
